all: accompagnements-numeriques-libres-annuaire.pdf accompagnements-numeriques-libres-resultats.pdf

couches_carto/REG_2019_CARTElette.shp:
	mkdir -p couches_carto
	# We could use the CARTElette package, but let's just call wget directly
	#echo 'CARTElette::charger_carte(COG=2019,nivsupra="REG",destfile="couches_carto")' | R --no-save
	cd couches_carto && for ext in dbf prj shp shx; do wget https://raw.githubusercontent.com/antuki/CARTElette/master/couches_carto/IGN/COG2019/REG_2019_CARTElette.$$ext; done

couches_carto/NUTS_RG_01M_2016_3035_LEVL_0.shp:
	mkdir -p couches_carto
	cd couches_carto && wget https://gisco-services.ec.europa.eu/distribution/v2/nuts/shp/NUTS_RG_01M_2016_3035_LEVL_0.shp.zip && unzip NUTS_RG_01M_2016_3035_LEVL_0.shp.zip

recensement.sqlite:
	@echo >&2
	@echo "Le fichier “recensement.sqlite” est manquant." >&2
	@echo "À vous de fournir les données du recensement…" >&2
	@echo >&2
	@exit 1

%.html: %.Rmd includes/colophon.html includes/extra.html includes/header.html couches_carto/REG_2019_CARTElette.shp couches_carto/NUTS_RG_01M_2016_3035_LEVL_0.shp recensement.sqlite
	echo 'rmarkdown::render("$<")' | R --no-save

%.pdf: %.html
	Rscript -e 'servr::httd(".", port=8421)' & httd=$$! ; \
		trap "kill $$httd" EXIT ; \
		pagedjs-cli/bin/paged -i 'http://127.0.0.1:8421/$<' -o '$@' --outline-tags '.main h1,.main h2:not(.hide-title)'
