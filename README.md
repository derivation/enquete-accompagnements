Enquête sur les accompagnements au numérique libre
==================================================

Voici le code utilisé pour fabriquer les documents publiés à l'issue de
l'enquête sur les accompagnements au numérique libre réalisé par
Framasoft à l'automne 2019.

Un des documents présente les résultats de l'enquête et l'autre
contient un annuaire d'accompagnements.

Outils
------

Les documents sont réalisés à l'aide du [langage R], de l'environnement
[R Markdown] et de [pagedown].

R Markdown permet de produire des documents en mélangeant du texte et
du code ce qui permet de mélanger interprétation, données et représentations
graphiques dans un même document source. R Markdown est en général
utilisé pour produire des PDF (via LaTeX) ou du HTML à destination des
navigateurs web. Le paquet pagedown permet lui de produire des PDF en
utilisant un navigateur web pour faire la mise en page, via la
bibliothèque [paged.js]. L'intérêt est de pouvoir faire la mise
en page via des CSS plutôt que des instructions plus obscures.

Le plus simple pour travailler sur des documents R Markdown
est d'utiliser le logiciel [R Studio], un environnement de développement
intégré pour le langage R.

[langage R]: https://www.r-project.org/
[R markdown]: https://rmarkdown.rstudio.com
[pagedown]: https://pagedown.rbind.io/
[paged.js]: https://www.pagedjs.org/
[R Studio]: https://rstudio.com/

Données
-------

Les données de l'enquête ont été extraite de [Framaforms.org] au format
[TSV], transformées en documents [YAML] pour être retravaillées plus
facilement, et enfin chargées dans une base [SQLite] (accédée via R).

Les scripts [Python] pour réaliser ces transformations se trouvent
dans le dossier `scripts/`.

[Framaforms.org]: https://framaforms.org/
[TSV]: https://en.wikipedia.org/wiki/Tab-separated_values
[YAML]: https://yaml.org/
[SQLite]: https://www.sqlite.org/
[Python]: https://www.python.org/

Voici un exemple de fiche au format YAML :

```yaml
---
webform_serial: 42
webform_time: 01/01/2019 - 01:23
utilisation_des_donn_es_collect_es: true
apparition_dans_l_annuaire: true
notification: true
nom: Agathe Louis
adresse_email: agathe.louis@example.org
email_dans_annuaire: true
en_quel_nom: structure
nom_structure: L’appartement du libre
site_web: https://appartement-du-libre.example.org/
site_web_dans_annuaire: true
forme_juridique: association
statut_des_interventions:
- benevole
- pro
formats:
- ateliers
- conf
- conseil
- formations_pro
autres_formats: |-
  Hackatons
public:
- neophyte
- quotidien
- expert
- autres_public
autres_public: |-
  Les bidouilleurs en tout genre
domaines:
- degooglisons
- contrib_ressources
- dev
- politique
- initiation
- licences
- outils_comm
- outils_crea
- outils_collab
- autres_domaines
autres_domaines: |-
  La cuisine
zones_geo:
- idf
autres_zones: |-
  L'appartement
lieu_ressource: true
latitude: 48.86674
longitude: 2.35576
ville: "Paris"
---
```

Installation des dépendances
----------------------------

Pour compiler les documents sur un système Debian, il faut installer
les paquets suivants :

```sh
sudo apt install --no-install-recommends \
	wget r-base build-essential libcurl4-openssl-dev libxml2-dev \
	libjq-dev gfortran libgfortran-10-dev libblas-dev liblapack-dev \
	libudunits2-dev libgdal-dev libprotobuf-dev protobuf-compiler \
	pandoc chromium
wget -O- https://deb.nodesource.com/setup_14.x | sudo bash -
sudo apt install nodejs 
```

On peut ensuite installer les paquets R dont nous avons besoin.
Après avoir lancé `R --no-save`, on peut saisir :

```R
install.packages("devtools")
install.packages("tidyverse")
install.packages("pagedown")
install.packages("RSQLite")
install.packages("ggfittext")
install.packages("ggforce")
install.packages("ggthemes")
install.packages("viridis")
install.packages("sf")
install.packages("cartography")
install.packages("servr")
```

Il est également nécessaire de récupérer les dépendances JavaScript, via :

```sh
npm install
```

Production des PDF
------------------

Si tout se passe bien, il est possible de générer les PDF avec
la commande :

```sh
make
```

Crédits
-------

Enquête et documents réalisés par Lunar et Mélissa de la Dérivation avec
l'aide d'Angie Gaudion, Pouhiou et Pierre-Yves Gosset.

Icônes par Adrien Coquet sous licence CC BY:
<https://thenounproject.com/coquet_adrien>

© Framasoft, 2020
<https://framasoft.org/>
sous licence Creative Commons Attribution – Partage dans les Mêmes
Conditions 4.0 International (CC BY-SA 4.0)
<https://creativecommons.org/licenses/by-sa/4.0/deed.fr>

Les fichiers dans `pagedjs-cli` proviennent initiallement du dépôt
<https://gitlab.pagedmedia.org/tools/pagedjs-cli>
Copyright (c) 2018-2020 Adam Hyde, Fabio Busatto, Fred Chason
sous license MIT.
