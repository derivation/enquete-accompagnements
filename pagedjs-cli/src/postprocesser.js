const PDFLib = require("pdf-lib");
const EventEmitter = require("events");

class PostProcesser extends EventEmitter {
  constructor(pdf) {
    super();

    if (!pdf) {
      throw "Must pass a PDF Buffer to PostProcesser";
    }
    this.pdf = pdf;
  }

  async load() {
    this.pdfDoc = await PDFLib.PDFDocument.load(this.pdf);
  }

  metadata(meta) {
    if (meta.keywords && typeof meta.keywords === "string") {
      meta.keywords = meta.keywords.split(",");
    }

    if (!meta.keywords) {
      meta.keywords = [];
    }

    // Overwrite Dates
    if (!(meta.creationDate instanceof Date)) {
      meta.creationDate = new Date();
    }
    meta.modDate = new Date();
    meta.metadataDate = new Date();

    // Get the existing Info
    if (!meta.creator) {
      meta.creator = this.pdfDoc.getCreator() + " + Paged.js";
    }

    if (!meta.producer) {
      meta.producer = this.pdfDoc.getProducer();
    }
    console.log("meta", meta);

    // Add meta
    this.addXmpMetadata(meta);
    this.updateInfoDict(meta);
  }

  updateInfoDict(meta) {
    if (meta.title) {
      this.pdfDoc.setTitle(meta.title);
    }

    if (meta.subject) {
      this.pdfDoc.setSubject(meta.subject);
    }

    if (meta.keywords && meta.keywords.length) {
      this.pdfDoc.setKeywords(meta.keywords);
    }

    if (meta.author) {
      this.pdfDoc.setAuthor(meta.author);
    }

    if (meta.creationDate) {
      this.pdfDoc.setCreationDate(meta.creationDate);
    }

    if (meta.modDate) {
      this.pdfDoc.setModificationDate(meta.modDate);
    }

    if (meta.creator) {
      this.pdfDoc.setCreator(meta.creator);
    }

    if (meta.producer) {
      this.pdfDoc.setProducer(meta.producer);
    }
  }

  addXmpMetadata(meta) {
    const whitespacePadding = new Array(20).fill(" ".repeat(100)).join("\n");
    const metadataXML = PDFLib.utf8Encode(`
      <?xpacket begin="" id="W5M0MpCehiHzreSzNTczkc9d"?>
        <x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.2-c001 63.139439, 2010/09/27-13:37:26">
          <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">

            <rdf:Description rdf:about="" xmlns:dc="http://purl.org/dc/elements/1.1/">
              <dc:format>application/pdf</dc:format>
              <dc:creator>
                <rdf:Seq>
                  <rdf:li>${meta.author}</rdf:li>
                </rdf:Seq>
              </dc:creator>
              <dc:title>
                 <rdf:Alt>
                    <rdf:li xml:lang="x-default">${meta.title}</rdf:li>
                 </rdf:Alt>
              </dc:title>
              <dc:subject>
                <rdf:Bag>
                  ${meta.keywords
                    .map((keyword) => `<rdf:li>${keyword}</rdf:li>`)
                    .join("\n")}
                </rdf:Bag>
              </dc:subject>
            </rdf:Description>

            <rdf:Description rdf:about="" xmlns:xmp="http://ns.adobe.com/xap/1.0/">
              <xmp:CreatorTool>${meta.creatorTool}</xmp:CreatorTool>
              <xmp:CreateDate>${meta.creationDate.toISOString()}</xmp:CreateDate>
              <xmp:ModifyDate>${meta.modDate.toISOString()}</xmp:ModifyDate>
              <xmp:MetadataDate>${meta.metadataDate.toISOString()}</xmp:MetadataDate>
            </rdf:Description>

            <rdf:Description rdf:about="" xmlns:pdf="http://ns.adobe.com/pdf/1.3/">
              <pdf:Subject>${meta.subject}</pdf:Subject>
              <pdf:Producer>${meta.producer}</pdf:Producer>
            </rdf:Description>

          </rdf:RDF>
        </x:xmpmeta>
        ${whitespacePadding}
      <?xpacket end="w"?>
    `.trim());

    const metadataStream = this.pdfDoc.context.stream(metadataXML, {
        Type: 'Metadata',
        Subtype: 'XML',
        Length: metadataXML.length,
      });

    const metadataStreamRef = this.pdfDoc.context.register(metadataStream);

    this.pdfDoc.catalog.set(PDFLib.PDFName.of("Metadata"), metadataStreamRef);
  }

  boxes(pages) {
    const pdfPages = this.pdfDoc.getPages();

    pdfPages.forEach((pdfPage, index) => {
      const page = pages[index];

      if (!page) {
        return; // page was not rendered
      }

      let { boxes } = page;

      if (Object.is(boxes.media, boxes.crop)) {
        return; // No bleed set
      }

      pdfPage.setTrimBox(boxes.crop.x, boxes.crop.y, boxes.crop.width + boxes.crop.x, boxes.crop.height + boxes.crop.y)
    });

  }

  updatePageBoxes(page) {
    console.log(page);
  }

  addPageLayout(name) {
    this.pdfDoc.catalog.set(PDFLib.PDFName.of("PageLayout"), PDFLib.PDFName.of(name));
  }

  addPageMode(name) {
    this.pdfDoc.catalog.set(PDFLib.PDFName.of("PageMode"), PDFLib.PDFName.of(name));
  }

  /**
   * Adds a table of content to the generated PDF
   *
   * Ideally this would not be required if Chromium would add this directly.
   * So if these bugs are closed this can probably be removed again:
   * - https://bugs.chromium.org/p/chromium/issues/detail?id=840455
   * - https://github.com/GoogleChrome/puppeteer/issues/1778
   *
   * This code is heavily based on @Hopding's comment at:
   * https://github.com/Hopding/pdf-lib/issues/127#issuecomment-502450179
   */
  addOutline(outlineSpec) {
    console.log("addOutline", outlineSpec);

    const getPageRefs = () => {
      const refs = [];
      this.pdfDoc.catalog.Pages().traverse((kid, ref) => {
        if (kid instanceof PDFLib.PDFPageLeaf) {
          refs.push(ref);
         }
      });
      return refs;
    };

    const createOutlineRefs = (outlineEntries) => {
      for (const outlineEntry of outlineEntries) {
        outlineEntry.ref = this.pdfDoc.context.nextRef();
        createOutlineRefs(outlineEntry.children);
      }
    };

    const createOutlineDicts = (parentRef, outlineEntries) => {
      outlineEntries.forEach((outlineEntry, index) => {
        outlineEntry.dict = this.pdfDoc.context.obj({
          Title: PDFLib.PDFHexString.fromText(outlineEntry.title),
          Parent: parentRef,
          Dest: [pageRefs[outlineEntry.page - 1], PDFLib.PDFName.of('Fit')],
          Prev: index > 0 ? outlineEntries[index - 1].ref : undefined,
          Next: index < outlineEntries.length - 1 ? outlineEntries[index + 1].ref : undefined,
          First: outlineEntry.children.length > 0 ? outlineEntry.children[0].ref : undefined,
          Last: outlineEntry.children.length > 0 ? outlineEntry.children[outlineEntry.children.length - 1].ref : undefined,
          // Note that we negate the count to get the tree closed upon loading
          Count: outlineEntry.children.length > 0 ? -outlineEntry.descendantCount : undefined,
        });
        createOutlineDicts(outlineEntry.ref, outlineEntry.children);
      });
    };

    const accountDescendants = (outlineEntries) => {
      let total = 0;
      for (const outlineEntry of outlineEntries) {
        accountDescendants(outlineEntry.children);
        outlineEntry.descendantCount = outlineEntry.children.length + outlineEntry.children.reduce((count, entry) => count + entry.descendantCount, 0);
        total += 1 + outlineEntry.descendantCount;
      }
      return total;
    };

    const assignDicts = (outlineEntries) => {
      for (const outlineEntry of outlineEntries) {
        this.pdfDoc.context.assign(outlineEntry.ref, outlineEntry.dict);
        assignDicts(outlineEntry.children);
      }
    };


    const pageRefs = getPageRefs();

    const outline = JSON.parse(JSON.stringify(outlineSpec));

    const outlinesDictRef = this.pdfDoc.context.nextRef();

    createOutlineRefs(outline);
    const totalCount = accountDescendants(outline);
    createOutlineDicts(outlinesDictRef, outline);
    assignDicts(outline);

    this.pdfDoc.catalog.set(PDFLib.PDFName.of("Outlines"),outlinesDictRef)
    this.pdfDoc.context.assign(outlinesDictRef, this.pdfDoc.context.obj({
        Type: 'Outlines',
        First: outline[0].ref,
        Last: outline[outline.length - 1].ref,
        Count: totalCount,
    }));
  }

  async save() {
    const pdfBytes = await this.pdfDoc.save();
    this.pdf = pdfBytes;
    return this.pdf;
  }
}

module.exports = PostProcesser;
