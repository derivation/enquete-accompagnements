```{r include=FALSE}
INDEX_PUBLICS <- TRUE
INDEX_DOMAINES <- TRUE
INDEX_FORMATS <- TRUE
INDEX_GEO <- TRUE

gen_index <- function(accompagnements) {
  accompagnements %>%
    filter(webform_serial %% data_resolution == 0) %>%
    pwalk(function(webform_serial, nom_index) {
      cat(paste0(" * [[", gsub("@", "&#x0040;", nom_index), "]{.nom-index}[XXX]{.page-number}](#serial-", webform_serial, ")", "\n"))
    })
}

```

# Index alphabétique {#index-alphabetique .index .no-hyphenation}

::: level2 :::

```{r echo=FALSE, eval=TRUE, include=TRUE, results='asis'}
accompagnements <- dbGetQuery(conn,
  "SELECT webform_serial,
          nom_index
     FROM recensement
    WHERE apparition_dans_l_annuaire = 1") %>%
  arrange(nom_index)
cat("\n")
gen_index(accompagnements)
cat("\n")
```

:::::::::::::

# Index par type de public {.index .has-toc .no-hyphenation}

```{r load-index-public, include=FALSE}
index_public_map <- public_map %>%
  filter(!public %in% c('autres_public')) %>%
  bind_rows(public_pro_map %>%
            transmute(public = public_pro, nom, nom_court)) %>%
  bind_rows(public_map %>%
            filter(public %in% c('autres_public')))
```

:::::: toc ::::::

```{r index-toc-public, echo=FALSE, eval=INDEX_GEO, include=TRUE, results='asis'}
index_public_map %>%
  pwalk(function(public, nom, nom_court) {
    prefix <- "*"
    if (public %in% public_pro_map$public_pro) {
      prefix <- "   -"
    }
    if (public == "pro") {
      cat(paste0(prefix, " ", nom, "\n"))
    } else {
      cat(paste0(prefix, " [", nom, "](#public-", public, ")\n"))
    }
  })
```

:::::::::::::::::

```{r index-public-all, echo=FALSE, eval=INDEX_PUBLICS, include=TRUE, results='asis'}
index_public_map %>%
  filter(public != "pro") %>%
  pwalk(function(public, nom, nom_court) {
    accompagnements <- dbGetQuery(conn,
      "SELECT webform_serial,
              nom_index
         FROM publics, recensement
        WHERE public = ?
          AND webform_serial = publics.serial
          AND apparition_dans_l_annuaire = 1
          AND lieu_ressource != 1",
      params=c(public)) %>%
      arrange(nom_index)
    cat(paste0("## ", nom, " {#public-", public, "}\n"))
    gen_index(accompagnements)
    cat("\n")
  })
```

# Index par domaine {.index .has-toc .no-hyphenation}

:::::: toc ::::::

```{r index-toc-domaine, echo=FALSE, eval=INDEX_GEO, include=TRUE, results='asis'}
domaine_map %>%
  pwalk(function(domaine, nom, nom_court) {
    cat(paste0("* [", nom, "](#domaine-", domaine, ")\n"))
  })
```

:::::::::::::::::

```{r index-domaine-all, echo=FALSE, eval=INDEX_DOMAINES, include=TRUE, results='asis'}

domaine_map %>%
  pwalk(function(domaine, nom, nom_court) {
    accompagnements <- dbGetQuery(conn,
      "SELECT webform_serial,
              nom_index
         FROM domaines, recensement
        WHERE domaine = ?
          AND webform_serial = domaines.serial
          AND apparition_dans_l_annuaire = 1
          AND lieu_ressource != 1",
      params=c(domaine)) %>%
      arrange(nom_index)
    cat(paste0("## ", nom, " {#domaine-", domaine, "}\n"))
    gen_index(accompagnements)
    cat("\n")
  })
```

# Index par format {#index-par-format .index .has-toc .no-hyphenation}

:::::: toc ::::::

```{r index-toc-format, echo=FALSE, eval=INDEX_GEO, include=TRUE, results='asis'}
format_map %>%
  filter(format != 'autres_formats') %>%
  pwalk(function(format, nom, nom_court) {
    cat(paste0("* [", nom, "](#format-", format, ")\n"))
  })
```

::::::::::::::::::

```{r index-all-format, echo=FALSE, eval=INDEX_FORMATS, include=TRUE, results='asis'}

format_map %>%
  filter(format != 'autres_formats') %>%
  pwalk(function(format, nom, nom_court) {
    accompagnements <- dbGetQuery(conn,
      "SELECT webform_serial,
              nom_index
         FROM formats, recensement
        WHERE format = ?
          AND webform_serial = formats.serial
          AND apparition_dans_l_annuaire = 1
          AND lieu_ressource != 1",
      params=c(format)) %>%
      arrange(nom_index)
    cat(paste0("## ", nom, " {#format-", format, " .", format, "}\n"))
    gen_index(accompagnements)
    cat("\n")
  })
```

# Index par zone d'intervention {#index-par-zone .index .has-toc .no-hyphenation}

```{r load-index-geo, include=FALSE}
out <- NULL

get_count <- function(zone_geo) {
  dbGetQuery(conn,
  "SELECT count(zone_geo) AS count
     FROM zones_geo
    WHERE zone_geo == ?",
  params=list(zone_geo))$count
}
```

:::::: toc ::::::

```{r index-toc-geo, echo=FALSE, eval=INDEX_GEO, include=TRUE, results='asis'}
noms_geo %>%
  filter(zone_geo != 'autres_zones' & get_count(zone_geo) > 0) %>%
  pwalk(function(zone_geo, nom) {
  prefix <- "*"
  if (zone_geo %in% reg_codes_metropole$zone_geo) {
    prefix <- "  -"
  }
  cat(paste0(prefix, " [", nom, "](#zone-", zone_geo, ")\n"))
})
```

::::::::::::::::::

```{r index-all-geo, echo=FALSE, eval=INDEX_GEO, include=FALSE}
out <- geo %>%
  right_join(noms_geo, by='zone_geo') %>%
  arrange(factor(zone_geo, levels = noms_geo$zone_geo)) %>%
  filter(zone_geo != 'autres_zones' & get_count(zone_geo) > 0) %>%
  pmap_chr(function(...) {
    env <- rlang::new_environment(data=rlang::list2(...), parent=knit_global())
    knit_child('_index_geo.Rmd', envir=env)
  })
```

`r paste(out, collapse='\n')`
