#!/usr/bin/env python3

# -*- encoding: utf-8 -*-

import csv
import sys
from pathlib import Path
import yaml
from collections import OrderedDict

from slugify import slugify

class literal(str):
    pass

# from https://stackoverflow.com/a/21912744
def ordered_dump(data, stream=None, Dumper=yaml.SafeDumper, **kwds):
    class OrderedDumper(Dumper):
        pass

    def _dict_representer(dumper, data):
        return dumper.represent_mapping(
            yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
            filter(lambda item: not (item[1].__class__ in (str, literal) and item[1] == ''), data.items()))
    OrderedDumper.add_representer(OrderedDict, _dict_representer)

    def _literal_presenter(dumper, data):
        return dumper.represent_scalar('tag:yaml.org,2002:str', data, style='|')
    OrderedDumper.add_representer(literal, _literal_presenter)

    return yaml.dump(data, stream, OrderedDumper, **kwds)

def handle_dump(dump_path, out_dir_path):
    with dump_path.open('r', encoding='utf-8') as dump_file:
        reader = csv.DictReader(dump_file, delimiter='\t')
        for row in reader:
            # Remove useless fields
            for field in 'webform_sid', 'webform_draft', 'webform_modified_time', 'webform_completed_time', 'webform_ip_address', 'webform_uid', 'webform_username':
                del row[field]

            # Turn comma-separated list into lists
            for field in 'statut_des_interventions', 'formats', 'public', 'public_pro', 'domaines', 'zones_geo':
                if row[field]:
                    row[field] = row[field].split(sep=',')

            # Prefer meaningful value to number
            row['en_quel_nom'] = 'structure' if row['en_quel_nom'] == '2' else 'nom_propre'

            # Strip string fields
            for field in 'nom', 'nom_structure', 'site_web':
                row[field] = row[field].strip()

            # Prefer integer to string
            row['webform_serial'] = int(row['webform_serial'])

            # Prefer boolean to string
            row['utilisation_des_donn_es_collect_es'] = row['utilisation_des_donn_es_collect_es'] == 'ok'
            row['notification'] = row['notification'] == 'notification'

            # Prefer boolean to number
            for field in 'apparition_dans_l_annuaire', 'email_dans_annuaire', 'site_web_dans_annuaire':
                row[field] = row[field] == '1'

            # Use block literals for long strings
            for field in 'catalogues', 'commentaires_libres', 'autre_forme_juridique_precision', 'autres_domaines', 'autres_geo', 'autres_pro', 'autres_public':
                row[field] = literal(row[field])

            # Rename this one for consistency
            row['autres_formats'] = literal(row['autres_modalites'])

            # Find best name depending on reply type
            name = row['nom_structure'] if row['en_quel_nom'] == 'structure' else row['nom']

            out_file_path = out_dir_path / '{:03d}-{}.yml'.format(row['webform_serial'], slugify(name))
            with out_file_path.open('x') as out_file:
                out_file.write(ordered_dump(row, explicit_start=True, explicit_end=True, default_flow_style=False, allow_unicode=True))
                out_file.write('\n\n')

def main():
    if len(sys.argv) != 3:
        print("Usage: {} dump.tsv out_directory".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    dump_path = Path(sys.argv[1])
    out_dir_path = Path(sys.argv[2])
    if not dump_path.exists():
        print("{} does not exist".format(dump_path), file=sys.stderr)
        sys.exit(1)
    if not (out_dir_path.is_dir() and next(out_dir_path.glob('*'), None) is None):
        print("{} is not an empty directory".format(out_dir_path), file=sys.stderr)
        sys.exit(1)
    handle_dump(dump_path, out_dir_path)


if __name__ == '__main__':
    main()
